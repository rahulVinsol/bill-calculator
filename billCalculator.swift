import Foundation

enum Category: String {
	case book, food, medicine, chocalate, stationary
	
	var isSalesTaxExempted: Bool {
		switch self {
			case .book, .food, .medicine: return true
			default: return false
		}
	}
}

struct Product: Equatable {
	let uuid: String
	let name: String
	var isImported: Bool
	var price: Double
	var category: Category
	
	init(uuid: String, name: String, isImported: Bool, price: Double, category: Category) {
		self.uuid = uuid
		self.name = name
		self.isImported = isImported
		self.price = price
		self.category = category
	}
	
	static func ==(lhs: Product, rhs: Product) -> Bool {
		lhs.uuid == rhs.uuid
	}
}

class CartItem: Equatable {
	let product: Product
	var quantity: Int
	
	init(product: Product, quantity: Int) {
		self.product = product
		self.quantity = quantity
	}
	
	static func ==(lhs: CartItem, rhs: CartItem) -> Bool {
		lhs.product == rhs.product
	}
}

class ShoppingCart {
	static let shared: ShoppingCart = ShoppingCart()
	
	private var items: Array<CartItem>
	
	private init() {
		items = []
	}
	
	private func findItem(product: Product) -> CartItem? {
		items.first(where: { $0.product == product })
	}
	
	func add(_ product: Product) {
		var cartItem = findItem(product: product)
		
		if cartItem == nil {
			cartItem = CartItem(product: product, quantity: 0)
			items.append(cartItem!)
		}
		
		cartItem!.quantity += 1
	}
	
	func remove(_ product: Product) {
		let cartItem = findItem(product: product)
		let newQuantity = (cartItem?.quantity ?? 0) - 1
		switch newQuantity {
			case let quant where quant < 0: return
			case 0: items.removeAll { $0.product == product }
			case let quant where quant > 0: cartItem?.quantity = quant
			default: break
		} 
	}
	
	func printBill() {
		var totalCost = 0.0
		for cartItem in items {
			var taxOnItem = 0.0
			
			let totalPriceItem = cartItem.product.price * Double(cartItem.quantity) 
			
			switch cartItem.product.category.isSalesTaxExempted {
				case true: taxOnItem += 0.0
				default:
					taxOnItem += totalPriceItem * 0.1
			}
			
			if cartItem.product.isImported {
				taxOnItem += totalPriceItem * 0.05
			}
			
			print("\(cartItem.product.name)\t\(String(format:"%.2f", cartItem.product.price))\t\(cartItem.quantity)\t\(String(format:"%.2f", taxOnItem))")
			
			totalCost += taxOnItem + totalPriceItem
		}
		totalCost.round()
		print("Total Cost => \(Int(totalCost))")
	}
}

let harryPotter = Product(uuid: UUID().uuidString, name: "Harry Potter", isImported: true, price: 3000, category: Category.book)

let burger1 = Product(uuid: UUID().uuidString, name: "Crispy Chicken", isImported: false, price: 70, category: Category.food)
let burger2 = Product(uuid: UUID().uuidString, name: "Veg Whopper", isImported: false, price: 140, category: Category.food)

let pfizer = Product(uuid: UUID().uuidString, name: "Pfizer", isImported: true, price: 2000, category: Category.medicine)

let dairyMilk = Product(uuid: UUID().uuidString, name: "Dairy Milk", isImported: false, price: 408, category: Category.chocalate)

let fiveStar = Product(uuid: UUID().uuidString, name: "Five Star", isImported: false, price: 500, category: Category.chocalate)

let geometryBox = Product(uuid: UUID().uuidString, name: "Geometry Box", isImported: false, price: 100, category: Category.stationary)

let shoppingCart = ShoppingCart.shared

shoppingCart.add(harryPotter)
shoppingCart.add(harryPotter)


shoppingCart.add(burger1)
shoppingCart.add(burger2)

shoppingCart.add(pfizer)

shoppingCart.add(dairyMilk)
shoppingCart.add(dairyMilk)
shoppingCart.add(fiveStar)

shoppingCart.add(geometryBox)

shoppingCart.printBill()
